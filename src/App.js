import React, {Component} from 'react';
import {observer} from "mobx-react";
// import DevTools from 'mobx-react-devtools';
import ScoreBox from './Components/ScoreBox';
import Header from "./Components/Header";
import PropTypes from 'prop-types';
import mediaQueries from './deviceMediaQueries';

import ClassicMenu from "./Components/ClassicMenu";
import SandwichMenu from "./Components/SandwichMenu";

import styled, {ThemeProvider} from "styled-components";

import theme from './themes/365default';

const StyledPage = styled.div`
    display: grid;
    grid-template-columns: 165px auto;
    grid-template-rows: 80px auto;
    grid-template-areas:
    "header header"
    "ClassicMenu score-box";
    height: 100vh;
    
    @media ${mediaQueries.tablet} {
        grid-template-areas:
            "header header"
            "score-box score-box";
    }
`;

@observer
class App extends Component {

    static childContextTypes = {
        store: PropTypes.object,
    };

    getChildContext() {
        return {
            store: this.props.store
        };
    }

    render() {
        const store = this.props.store;
        const toggleSandwichMenu = store.toggleSandwichMenu;

        return (
            <ThemeProvider theme={theme}>
                <StyledPage>
                    <Header actions={{toggleSandwichMenu}}/>
                    <ClassicMenu/>
                    {store.showSandwichMenu && <SandwichMenu actions={{toggleSandwichMenu}}/>}
                    <ScoreBox gameData={store.gameData} gamesByCompetitions={store.gamesByCompetitions}/>
                    {/*<DevTools/>*/}
                </StyledPage>
            </ThemeProvider>
        );
    }
}


export default App;
