import React from 'react';

import styled from "styled-components";

const StyledMenuItem = styled.div`
    font-size: 1.3em;
    padding: 15px;
`;


class StaticMenu extends React.Component {
    render() {
        return (<div>
            <StyledMenuItem>Menu item #1</StyledMenuItem>
            <StyledMenuItem>Menu item #2</StyledMenuItem>
            <StyledMenuItem>Menu item #3</StyledMenuItem>
            <StyledMenuItem>Menu item #4</StyledMenuItem>
            <StyledMenuItem>Menu item #5</StyledMenuItem>
            <StyledMenuItem>Menu item #6</StyledMenuItem>
            <StyledMenuItem>Menu item #7</StyledMenuItem>
        </div>);
    }
}

export default StaticMenu;