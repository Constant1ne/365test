import React from 'react';
import {observer} from "mobx-react";

import StaticMenu from "./StaticMenu";
import {MenuIcon} from "./StyledComponents";

import styled from "styled-components";

const StyledSandwichMenu = styled.div`
    grid-column-start: 1;
    grid-column-end: 2;
    grid-row-start: 1;
    grid-row-end: 3;
    z-index: 2;
    background-color: ${props => props.theme.sandwichMenuBackgroundColor};
    min-height: 100%;
`;

@observer
class SandwichMenu extends React.Component {
    render() {
        return (
            <StyledSandwichMenu>
                <div onClick={this.props.actions.toggleSandwichMenu}>
                    <MenuIcon src={"./menu-icon.png"} alt={"menu"}/>
                </div>
                <StaticMenu/>
            </StyledSandwichMenu>);
    }
}

export default SandwichMenu;