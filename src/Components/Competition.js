import React from 'react';

import config from '../config';
import Game from "./Game/Game";

import {observer} from "mobx-react";

import styled from "styled-components";
import mediaQueries from "../deviceMediaQueries";

const StyledCompetition = styled.div`
    width: 587px;
    background-color: ${props => props.theme.competitionBackgroundColor};
    margin: 10px;
    border-radius: 10px;
    border: 1px solid #272f35;
    box-shadow: 3px 3px 15px 0px rgba(0,0,0,0.4);
    
    @media ${mediaQueries.mobile} {
        width: 310px;
    }
`;

const CompetitionHeader = styled.div`
    display: flex;
    align-items: center;
    height: 40px;
    color: ${props => props.theme.competitionHeaderColor};
`;

const CountryFlag = styled.img`
    width: 32px;
    height: 32px;
    margin: 10px;
`;

const Competition = observer(({competition, games}) => {

    return (
        <StyledCompetition>
            <CompetitionHeader>
                <CountryFlag
                        src={`${config.competitions_img_url}${competition.CID}`}
                        alt={competition.Name}/>
                {competition.Name}
            </CompetitionHeader>
            <div>
                {games.map(game => <Game game={game} key={game.ID}/>)}
            </div>
        </StyledCompetition>
    );
});

export default Competition;