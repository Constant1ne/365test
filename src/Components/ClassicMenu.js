import React from 'react';
import StaticMenu from "./StaticMenu";

import styled from "styled-components";
import mediaQueries from "../deviceMediaQueries";

const StyledClassicMenu = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${props => props.theme.classicMenuBackgroundColor};
    grid-area: ClassicMenu;
    
    @media ${mediaQueries.tablet} {
        display: none;
    }
`;

class ClassicMenu extends React.Component {
    render() {
        return (
            <StyledClassicMenu>
                <StaticMenu/>
            </StyledClassicMenu>);
    }
}

export default ClassicMenu;