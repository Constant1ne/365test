import React from 'react';
import Competition from "./Competition";
import {observer} from "mobx-react";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCog} from '@fortawesome/free-solid-svg-icons'

import styled from "styled-components";

const StyledScoreBox = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 5;
    align-items: center;
    justify-content: center;
    background-color: ${props => props.theme.scoreBoxBackgroundColor};
    grid-area: score-box;
`;

@observer
class ScoreBox extends React.Component {
    render() {
        return (
            <StyledScoreBox>
                {!this.props.gameData && <FontAwesomeIcon icon={faCog} spin size="6x"/>}
                {this.props.gameData && this.props.gameData.Competitions
                    .filter(competition => this.props.gamesByCompetitions[competition.ID] !== undefined)
                    .map(competition =>
                        <Competition competition={competition}
                                     games={this.props.gamesByCompetitions[competition.ID]}
                                     key={competition.ID}
                        />
                    )}
            </StyledScoreBox>);
    }
}

export default ScoreBox;