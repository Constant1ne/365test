import styled from "styled-components";
import mediaQueries from "../deviceMediaQueries";

const MenuIcon = styled.img`
    padding: 20px;
    display: none;
    
    @media ${mediaQueries.tablet} {
        display: flex;
    }
`;

export { MenuIcon };