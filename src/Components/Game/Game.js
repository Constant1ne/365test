import React from 'react';
import {observer} from "mobx-react";

import styled from "styled-components";

import mediaQueries from "../../deviceMediaQueries";

import config from '../../config';

import LeftTeam from './LeftTeam';
import RightTeam from "./RightTeam";

const Game = observer(({game, actions}) => {
    const isGameFinished = !game.Active &&
        game.Scrs !== undefined &&
        game.Scrs[0] !== -1 && game.Scrs[1] !== -1;

    const gameTime = game.STime !== undefined ? game.STime.split(' ')[1] : "99:99";



    const gameInfo = game.Scrs !== undefined && game.Scrs[0] !== -1 && game.Scrs[1] !== -1 ?
        <GameScore>{`${findValidTeamScore(game.Scrs, 0)} - ${findValidTeamScore(game.Scrs, 1)}`}</GameScore> :
        <GameDate className={"gameDate"}>{gameTime}</GameDate>;

    return (
        <StyledGame>
            <LeftTeam teamName={game.Comps[0].Name}
                teamFlagUrl={`${config.competitors_img_url}${game.Comps[0].ID}`} />

            <GameInfo>
                {gameInfo}
                {game.Active && <LiveResult>
                    <LiveResultText>
                        {`LIVE - ${game.GTD}`}
                    </LiveResultText>
                </LiveResult>}
                {isGameFinished && <FinishedGame>Finished</FinishedGame>}
            </GameInfo>

            <RightTeam teamName={game.Comps[1].Name}
                      teamFlagUrl={`${config.competitors_img_url}${game.Comps[1].ID}`} />
        </StyledGame>
    );
});

function findValidTeamScore (scrs, index) {
    if(index < 0) return 0;

    if(scrs[index] >= 0) {
        return scrs[index];
    }

    return findValidTeamScore(scrs, index - 2);
}

const StyledGame = styled.div`
    width: 585px;
    height: 80px;
    display: flex;
    align-content: center;
    background-color: ${props => props.theme.gameBackgroundColor};
    margin: 1px;
    
    &:last-child {
        border-radius: 0px 0px 10px 10px;
    }
    
    @media ${mediaQueries.mobile} {
        width: 308px;
        height: 100px;
    }
`;

const GameInfo = styled.div`
    display: flex;
    flex-direction: column;
    width: 145px;
    flex-grow: 1;
    color: ${props => props.theme.gameInfoColor};
    align-items: center;
    justify-content: center;
    text-align: center;
    
    @media ${mediaQueries.mobile} {
        width: 68px;
    }
`;


const GameScore = styled.div`
    font-size: 2em;
    font-family: 'Roboto Condensed', sans-serif;
    font-weight: bold;
    
    @media ${mediaQueries.mobile} {
        font-size: 1.3em;
    }
`;

const GameDate = styled.div`
    font-size: 1.5em;
    font-family: 'Roboto Condensed', sans-serif;
    font-weight: bold;
    
    @media ${mediaQueries.mobile} {
        font-size: 1.2em;
    }
`;

const LiveResult = styled.div`
    background-color: ${props => props.theme.liveResultBackgroundColor};
    border-radius: 25px;
    padding: 2px 5px 2px 5px;
    font-family: 'Roboto', sans-serif;
    font-weight: 300;
    
    @media ${mediaQueries.mobile} {
        height: 6px;
    }
`;

const LiveResultText = styled.div`
    @media ${mediaQueries.mobile} {
        display: none;
    }
`;

const FinishedGame = styled.div`
    color: ${props => props.theme.finishedGameColor};
`;

export default Game;