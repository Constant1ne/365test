import React from 'react';
import styled from "styled-components";

import {TeamFlag} from "./TeamFlag";
import mediaQueries from "../../deviceMediaQueries";

const LeftTeam = ({teamName, teamFlagUrl}) => {
    return (
        <StyledLeftTeam>
            <TeamFlag src={`${teamFlagUrl}`}
                      alt={teamName}
            />
            {teamName}
        </StyledLeftTeam>
    );
};

const StyledLeftTeam = styled.div`
    display: flex;
    width: 220px;
    flex-grow: 3;
    align-items: center;
    
    @media ${mediaQueries.mobile} {
        width: 120px;
    }
`;

export default LeftTeam;