import React from 'react';
import styled from "styled-components";

import {TeamFlag} from "./TeamFlag";
import mediaQueries from "../../deviceMediaQueries";

const RightTeam = ({teamName, teamFlagUrl}) => {
    return (
        <StyledRightTeam>
            <TeamFlag src={`${teamFlagUrl}`}
                      alt={teamName}
            />
            {teamName}
        </StyledRightTeam>
    );
};

const StyledRightTeam = styled.div`
    display: flex;
    width: 220px;
    flex-grow: 3;
    flex-direction: row-reverse;
    align-items: center;
    text-align: right;
    
    @media ${mediaQueries.mobile} {
        width: 120px;
    }
`;

export default RightTeam;