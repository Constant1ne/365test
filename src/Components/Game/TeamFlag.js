import styled from "styled-components";
import mediaQueries from "../../deviceMediaQueries";

const TeamFlag = styled.img`
    width: 32px;
    height: 32px;
    margin: 10px;
    
    @media ${mediaQueries.mobile} {
        margin: 5px;
    }
`;

export {TeamFlag};