import React from 'react';
import { MenuIcon } from './StyledComponents';
import styled from "styled-components";

const Logo = styled.img`
    padding: 20px;
`;

const StyledHeader = styled.div`
    display: flex;
    align-items: center;
    background-color: ${props => props.theme.headerBackgroundColor};
    grid-area: header;
`;

class Header extends React.Component {
    render() {
        return (
            <StyledHeader>
                <div onClick={this.props.actions.toggleSandwichMenu}>
                    <MenuIcon src={"./menu-icon.png"} alt={"menu"}/>
                </div>
                <Logo src={"http://www.365scores.com/LPResources/img/logo_77x42.png"}
                     alt={"365scores"}/>
            </StyledHeader>
        );
    }
}

export default Header;