export default {
    'headerBackgroundColor': '#081621',

    'sandwichMenuBackgroundColor': '#2e373e',
    'classicMenuBackgroundColor': '#2e373e',

    'scoreBoxBackgroundColor': '#21282d',

    'competitionBackgroundColor': '#272f35',
    'competitionHeaderColor': '#81d4fa',

    'gameBackgroundColor': '#2e373e',
    'gameInfoColor': '#81d4fa',

    'liveResultBackgroundColor': '#FF0000',

    'finishedGameColor': '#5d93ad',
}