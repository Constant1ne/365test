export default {
    mobile: 'all and (max-width: 585px)',
    tablet: 'all and (max-width: 767px)'
};