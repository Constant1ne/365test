import {observable, computed, action, configure} from 'mobx';

import config from "../config";

configure({enforceActions: "strict"});

class MobxApplicationStore {
    @observable showSandwichMenu;
    @observable gameData;

    constructor() {
        this.updateGameScores(true);

        this.toggleSandwichMenu = this.toggleSandwichMenu.bind(this);

        setInterval(() => this.updateGameScores(), 5000);
    }

    @computed get gamesByCompetitions() {
        if (this.gameData === undefined) {
            return {};
        }

        const games = this.gameData.Games;

        return Object.keys(games).reduce((acc, gameId) => {
            const current = games[gameId];
            if((current.Scrs !== undefined || current.STime !== undefined) && current.Comps !== undefined) {
                acc[current.Comp] = acc[current.Comp] !== undefined ? [...acc[current.Comp], current] : [current];
            }
            return acc;
        }, {});
    }

    fakeScoreGame(gameId) {

        this.gameData.Games[gameId].Scrs = [1,1, ...this.gameData.Games[gameId].Scrs];
    }

    @action
    toggleSandwichMenu() {
        this.showSandwichMenu = !this.showSandwichMenu;
    }

    @action
    updateGameScores(isInitRequest = false) {
        if(!isInitRequest && (!this.gameData || !this.gameData.LastUpdateID)) {
            return;
        }

        const updateInfo = isInitRequest ? '' : `&UID=${this.gameData.LastUpdateID}`;

        fetch(`${config.api_request_url}${updateInfo}`)
            .then(response => response.json())
            .then(action("receiving update ", data => {

                console.log(data);

                //Array to Map
                data.Games = data.Games !== undefined ? data.Games : [];
                data.Games = data.Games.reduce((acc, curr) => {
                    acc[curr.ID] = curr;
                    return acc;
                }, {});

                if (isInitRequest) {
                    this.gameData = data;
                } else {
                    const newState = Object.assign({}, this.gameData, {LastUpdateID: data.LastUpdateID});
                    const updatedGames = data.Games;
                    Object.keys(updatedGames).forEach( gameId => {
                        newState.Games[gameId] = this.mergeGameInfo(newState.Games[gameId], updatedGames[gameId]);
                    });
                    this.gameData = newState;
                }
            }));
    }

    mergeGameInfo(sourceGame, updatedGameInfo) {
        let gameScores;
        if(updatedGameInfo && updatedGameInfo.Scrs !== undefined) {
            if(sourceGame && sourceGame.Scrs !== undefined) {
                gameScores = [...updatedGameInfo.Scrs, ...sourceGame.Scrs];
            } else {
                gameScores = updatedGameInfo.Scrs;
            }
            updatedGameInfo.Scrs = gameScores;
        }

        return Object.assign({}, sourceGame, updatedGameInfo);
    }
}

export default new MobxApplicationStore();