import React from 'react';
import PropTypes from 'prop-types';

const storeProvider = (extraProps) => (Component) => {
    return class extends React.Component {
        static displayName = `${Component.name}Container`;
        static contextTypes = {
            store: PropTypes.object,
        };

        render() {
            return <Component
                {...this.props}
                {...extraProps(this.context.store, this.props)}
                store={this.context.store} />;
        }
    };
};

export default storeProvider;


//POSSIBLE USAGE
//  function extraProps(store, originalProps) {
//      return {
//          author: store.lookupAuthor(originalProps.article.authorId),
//      };
//  }
//
//  export default storeProvider(extraProps)(Article);