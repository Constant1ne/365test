import React from 'react';
import ReactDOM from 'react-dom';
import './index2.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import store from './store/MobxApplicationStore';

window.store = store;

ReactDOM.render(<App store={store}/>, document.getElementById('root'));
registerServiceWorker();
